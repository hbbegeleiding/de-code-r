function parseMorse(morse) {
	morse = morse.replace(/\s+/ig, ' ');
	morse = morse.replace(/[-_—]/ig, '—');
	morse = morse.replace(/[.·]/ig, '·');

	let words = morse.split('/');
	let message = '';
	_.each(words, (word) => {
		word = word.trim();
		let letters = word.split(' ');
		_.each(letters, (letter) => {
			message += Morse.morseCodes[letter] || '?';
		});
		message += ' ';
	});

	return message;
}

Template.morse_decode.onCreated(function() {
	this.showMorse = new ReactiveVar(false);
});

Template.morse_decode.helpers({
	showMorse: function() {
		return Template.instance().showMorse.get();
	}
});

Template.morse_decode.events({
	"click .messageTitle": function(e) {
		e.preventDefault();
		$('#morse').val('');
		$('#message').html('');
	},
	"keyup #morse": function(e) {
		let morse = $('#morse').val() || '';
		var message = parseMorse(morse);
		$('#message').html(message);
	},
	"click .show-morse": function(e, template) {
        let morse = $('#morse').val() || '';
        var message = parseMorse(morse);
        $('#message').html(message);
		template.showMorse.set(!template.showMorse.get())
	}
});
