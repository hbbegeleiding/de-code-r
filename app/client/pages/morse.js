Template.morse.helpers({
    allowSharing: function() {
        return Meteor.isCordova;
    }
});

Template.morse.events({
	"click .messageTitle": function(e) {
		e.preventDefault();
		$('#message').val('');
		$('#morse').html('');
	},
	"keyup #message": function(e) {
		var message = $('#message').val() || {};
		//console.log(message);
		var morse = '';
		for(var i = 0; i < message.length; i++) {
			var char = message[i].toLowerCase();
			if (Morse.charCodes[char]) {
				morse += Morse.charCodes[char] + ' ';
			} else {
				morse += char + ' ';
			}
		}
		$('#morse').html(morse);
	},
	"click .morse-share": function(e) {
		e.preventDefault();
		if (window.plugins && window.plugins.socialsharing) {
			window.plugins.socialsharing.share($('#morse').html());
		} else {
			alert('Plugin niet goed geinstalleerd');
		}
	}
});
