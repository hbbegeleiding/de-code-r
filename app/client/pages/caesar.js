Template.caesar.onCreated(function() {
	this.rotation = new ReactiveVar(Number(localStorage.getItem('CaesarRotation') || 13));
});

Template.caesar.onRendered(function() {
	this.autorun(() => {
		localStorage.setItem('CaesarRotation', this.rotation.get());
	});
});

Template.caesar.helpers({
	getRotation: function() {
		return Template.instance().rotation.get() * (360 / 26);
	},
    allowSharing: function() {
        return Meteor.isCordova;
    }
});

Template.caesar.events({
	"click .messageTitle": function(e) {
		e.preventDefault();
		$('#message').val('');
		$('#caesar').html('');
	},
	"keyup #message": function(e, template) {
		var message = $('#message').val() || '';
		$('#caesar').html(Caesar(message, 26 - template.rotation.get()));
	},
	"click .caesar-share": function(e) {
		e.preventDefault();
		if (window.plugins && window.plugins.socialsharing) {
			window.plugins.socialsharing.share($('#caesar').html());
		} else {
			alert('Plugin niet goed geinstalleerd');
		}
	},
	"click #rot": function(e, template) {
		var div = $(e.currentTarget);
		//console.log(e.pageX >= (div.offset().left + div.width()/2) ? 'clicked right' : 'clicked left');
		if (e.pageX >= (div.offset().left + div.width()/2)) {
			// right
			let currentRotation = template.rotation.get();
			template.rotation.set(currentRotation < 26 ? currentRotation + 1 : 1);
		} else {
			// left
			let currentRotation = template.rotation.get();
			template.rotation.set(currentRotation > 1 ? currentRotation - 1 : 26);
		}
		var message = $('#message').val() || '';
		$('#caesar').html(Caesar(message, 26 - template.rotation.get()));
	}
});
