Template.caesar_decode.onCreated(function() {
	this.showCaesar = new ReactiveVar(false);
	this.rotation = new ReactiveVar(Number(localStorage.getItem('CaesarRotation') || 13));
});

Template.caesar_decode.onRendered(function() {
	this.autorun(() => {
		localStorage.setItem('CaesarRotation', this.rotation.get());
	});
});

Template.caesar_decode.helpers({
	showCaesar: function() {
		return Template.instance().showCaesar.get();
	},
	getRotation: function() {
		return Template.instance().rotation.get() * (360 / 26);
	}
});

Template.caesar_decode.events({
	"click .messageTitle": function(e) {
		e.preventDefault();
		$('#message').val('');
		$('#caesar').html('');
	},
	"keyup #caesar": function(e, template) {
		let caesar = $('#caesar').val() || '';
		var message = Caesar(caesar, template.rotation.get());
		$('#message').html(message);
	},
	"click .show-caesar": function(e, template) {
        let caesar = $('#caesar').val() || '';
        var message = Caesar(caesar, template.rotation.get());
        $('#message').html(message);
		template.showCaesar.set(!template.showCaesar.get())
	},
	"click #rot": function(e, template) {
		var div = $(e.currentTarget);
		//console.log(e.pageX >= (div.offset().left + div.width()/2) ? 'clicked right' : 'clicked left');
		if (e.pageX >= (div.offset().left + div.width()/2)) {
			// right
			let currentRotation = template.rotation.get();
			template.rotation.set(currentRotation < 26 ? currentRotation + 1 : 1);
		} else {
			// left
			let currentRotation = template.rotation.get();
			template.rotation.set(currentRotation > 1 ? currentRotation - 1 : 26);
		}
		let caesar = $('#caesar').val() || '';
		var message = Caesar(caesar, template.rotation.get());
		$('#message').html(message);
	}
});
