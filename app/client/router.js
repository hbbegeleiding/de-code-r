Router.configure({
	layoutTemplate: 'layout'
});

Router.route('/', function () {
	this.render('home', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/morse', function () {
	this.render('morse', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/morse_decode', function () {
	this.render('morse_decode', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/caesar', function () {
	this.render('caesar', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/caesar_decode', function () {
	this.render('caesar_decode', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/boek', function () {
	this.render('boek', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/sas', function () {
	this.render('sas', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/jas', function () {
	this.render('jas', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});

Router.route('/fred', function () {
	this.render('fred', {
		data: function () {
			//return Items.findOne({_id: this.params._id});
		}
	});
});
