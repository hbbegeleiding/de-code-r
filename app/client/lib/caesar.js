/**
 * Rotate string rot chars
 *
 * To translate backe, rotate 26 - rot
 *
 * @param s
 * @param rot
 * @returns {XML|string|void}
 */
Caesar = function(s, rot) {
	s = s || '';
	if (rot === undefined) {
		rot = 13;
	}
	return s.replace(/[a-zA-Z]/g, function(c) {
		return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + rot) ? c : c-26);
	});
};